﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Oracle.ManagedDataAccess.Client;

namespace Generator_danych
{
    public class Autorzy
    {
        public Autorzy() { 

        }

        private int _ID;
        private string _imie;
        private string _nazwisko;
        private string _data_urodzenia;

        public int ID { get => _ID; set => _ID = value; }
        public string Imie { get => _imie; set => _imie = value; }
        public string Nazwisko { get => _nazwisko; set => _nazwisko = value; }
        public string Data_urodzenia { get => _data_urodzenia; set => _data_urodzenia = value; }

        public string GenerateInsert()
        {
            return string.Format("INSERT INTO autorzy (ID_AUTORA, IMIE, NAZWISKO, DATA_URODZENIA) VALUES ({0}, '{1}', '{2}', '{3}');", ID, Imie, Nazwisko, Data_urodzenia );
        }

        public void GenerateRandomData(int lastID, string[] names, string[] surnames)
        {
            Random rand = new Random();
            _ID = lastID + 1;
            _imie = names[rand.Next(1, names.Length)];
            _nazwisko = surnames[rand.Next(1, surnames.Length)];
            _data_urodzenia = Date.getDate(rand.Next(1, 30), rand.Next(1, 12), rand.Next(1950, 2000));
        }
    }

    public class Ksiazka
    {
        public Ksiazka(){
            
        }

        private int _ID_ksiazki;
        private int _ID_autora;
        private string _tytul;
        private string _gatunek;
        private string _rok_wydania;
        private string _wydawnictwo;

        public int ID_ksiazki { get => _ID_ksiazki; set => _ID_ksiazki = value; }
        public int ID_autora { get => _ID_autora; set => _ID_autora = value; }
        public string Tytul { get => _tytul; set => _tytul = value; }
        public string Gatunek { get => _gatunek; set => _gatunek = value; }
        public string Rok_wydania { get => _rok_wydania; set => _rok_wydania = value; }
        public string Wydawnictwo { get => _wydawnictwo; set => _wydawnictwo = value; }
        public string GenerateInsert()
        {
            return string.Format("INSERT INTO ksiazka (ID_KSIAZKI, ID_AUTORA, TYTUL, GATUNEK, ROK_WYDANIA, WYDAWNICTWO) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}');", _ID_ksiazki, _ID_autora, _tytul, _gatunek, _rok_wydania, _wydawnictwo);
        }

        public void GenerateRandomData(int lastID, string[] names, List<int> id_autora_list)
        {
            Random rand = new Random();
            _ID_ksiazki = lastID + 1;
            _ID_autora = Randomizer.GetRandomItemFromList(id_autora_list);
            _tytul = names[rand.Next(1, names.Length)] + " i jego przygody w wielkim świecie";
            _gatunek = "Powieść przygodowa";
            _rok_wydania = Date.getDate(rand.Next(1, 30), rand.Next(1, 12), rand.Next(1950, 2019));
            _wydawnictwo = "Nowa era";
        }
    }

    public class Egzemplarz
    {
        public Egzemplarz()
        {

        }

        private int _ID_egzemplarza;
        private int _ID_ksiazki;
        private string _stan_egzemplarza;
        private int _dostepnosc;

        public int ID_egzemplarza { get => _ID_egzemplarza; set => _ID_egzemplarza = value; }
        public int ID_ksiazki { get => _ID_ksiazki; set => _ID_ksiazki = value; }
        public string Stan_egzemplarza { get => _stan_egzemplarza; set => _stan_egzemplarza = value; }
        public int Dostepnosc { get => _dostepnosc; set => _dostepnosc = value; }
        public string GenerateInsert()
        {
            return string.Format("INSERT INTO egzemplarz (ID_EGZEMPLARZA, ID_KSIAZKI, STAN_EGZEMPLARZA, DOSTEPNOSC) VALUES ({0}, {1}, '{2}', {3} );", _ID_egzemplarza, _ID_ksiazki, _stan_egzemplarza, _dostepnosc);
        }

        public void GenerateRandomData(int lastID, List<int> id_ksiazki_list)
        {
            _ID_egzemplarza = lastID + 1;
            _ID_ksiazki = Randomizer.GetRandomItemFromList(id_ksiazki_list);
            _stan_egzemplarza = "Dobry";
            _dostepnosc = 1;
        }
    }

    public class Klient
    {
        public Klient()
        {

        }

        private int _ID_klienta;
        private string _imie;
        private string _nazwisko;
        private string _data_urodzenia;
        private string _adres_email;
        private int _telefon;
        private string _login;
        private string _haslo;

        public int ID_klienta { get => _ID_klienta; set => _ID_klienta = value; }
        public string Imie { get => _imie; set => _imie = value; }
        public string Nazwisko { get => _nazwisko; set => _nazwisko = value; }
        public string Data_urodzenia { get => _data_urodzenia; set => _data_urodzenia = value; }
        public string Adres_email { get => _adres_email; set => _adres_email = value; }
        public int Telefon { get => _telefon; set => _telefon = value; }
        public string Login { get => _login; set => _login = value; }
        public string Haslo { get => _haslo; set => _haslo = value; }
        public string GenerateInsert()
        {
            return string.Format("INSERT INTO klient (ID_KLIENTA, IMIE, NAZWISKO, DATA_URODZENIA, ADRES_EMAIL, TELEFON, LOGIN, HASLO) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', {5}, '{6}', '{7}');", _ID_klienta, _imie, _nazwisko, _data_urodzenia, _adres_email, _telefon, _login, _haslo);
        }

        public void GenerateRandomData(int lastID, string[] names, string[] surnames)
        {
            Random rand = new Random();
            _ID_klienta = lastID + 1;
            _imie = names[rand.Next(1, names.Length)];
            _nazwisko = surnames[rand.Next(1, names.Length)];
            _data_urodzenia = Date.getDate(rand.Next(1, 30), rand.Next(1, 12), rand.Next(1950, 2000));
            _adres_email = string.Format("{0}.{1}@poczta.pl", _imie, _nazwisko);
            _telefon = rand.Next(100000000, 999999999);
            _login = string.Format("{0}.{1}", _imie, _nazwisko);
            _haslo = rand.Next(100000, 999999).ToString();
        }
    }

    public class Pracownik
    {
        public Pracownik()
        {

        }

        private int _ID_pracownika;
        private string _imie;
        private string _nazwisko;
        private string _stanowisko;
        private string _login;
        private string _haslo;

        public int ID_pracownika { get => _ID_pracownika; set => _ID_pracownika = value; }
        public string Imie { get => _imie; set => _imie = value; }
        public string Nazwisko { get => _nazwisko; set => _nazwisko = value; }
        public string Stanowisko { get => _stanowisko; set => _stanowisko = value; }
        public string Login { get => _login; set => _login = value; }
        public string Haslo { get => _haslo; set => _haslo = value; }

        public string GenerateInsert()
        {
            return string.Format("INSERT INTO pracownik (ID_PRACOWNIKA, IMIE, NAZWISKO, STANOWISKO, LOGIN, HASLO) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}');", _ID_pracownika, _imie, _nazwisko, _stanowisko, _login, _haslo);
        }

        public void GenerateRandomData(int lastID, string[] names, string[] surnames)
        {
            Random rand = new Random();
            _ID_pracownika = lastID + 1;
            _imie = names[rand.Next(1, names.Length)];
            _nazwisko = surnames[rand.Next(1, names.Length)];
            _stanowisko = "Bibliotekarz";
            _login = string.Format("{0}.{1}", _imie, _nazwisko);
            _haslo = rand.Next(100000, 999999).ToString();
        }
    }

    public class Wypozyczenie
    {
        public Wypozyczenie()
        {

        }

        private int _ID_wypozyczenia;
        private int _ID_pracownika;
        private int _ID_egzemplarza;
        private int _ID_klienta;
        private string _stan_po_zwrocie;
        private string _data_wypozyczenia;
        private string _termin_zwrotu;
        private string _data_oddania;

        public int ID_wypozyczenia { get => _ID_wypozyczenia; set => _ID_wypozyczenia = value; }
        public int ID_pracownika { get => _ID_pracownika; set => _ID_pracownika = value; }
        public int ID_egzemplarza { get => _ID_egzemplarza; set => _ID_egzemplarza = value; }
        public int ID_klienta { get => _ID_klienta; set => _ID_klienta = value; }
        public string Stan_po_zwrocie { get => _stan_po_zwrocie; set => _stan_po_zwrocie = value; }
        public string Data_wypozyczenia { get => _data_wypozyczenia; set => _data_wypozyczenia = value; }
        public string Termin_zwrotu { get => _termin_zwrotu; set => _termin_zwrotu = value; }
        public string Data_oddania { get => _data_oddania; set => _data_oddania = value; }
        public string GenerateInsert()
        {
            return string.Format("INSERT INTO wypozyczenie (ID_WYPOZYCZENIA, ID_PRACOWNIKA, ID_EGZEMPLARZA, ID_KLIENTA, STAN_PO_ZWROCIE, DATA_WYPOZYCZENIA, TERMIN_ZWROTU, DATA_ODDANIA) VALUES ({0},{1},{2},{3},'{4}','{5}','{6}','{7}');", _ID_wypozyczenia, _ID_pracownika, _ID_egzemplarza, _ID_klienta, _stan_po_zwrocie, _data_wypozyczenia, _termin_zwrotu, _data_oddania);
        }

        public void GenerateRandomData(int lastID, List<int> id_pracownika_list, List<int> id_egzemplarza_list, List<int> id_klienta_list)
        {
            Random rand = new Random();
            _ID_wypozyczenia = lastID + 1;
            _ID_pracownika = Randomizer.GetRandomItemFromList(id_pracownika_list);
            _ID_egzemplarza = Randomizer.GetRandomItemFromList(id_egzemplarza_list);
            _ID_klienta = Randomizer.GetRandomItemFromList(id_klienta_list);
            _stan_po_zwrocie = "Dobry";
            _data_wypozyczenia = Date.getDate(rand.Next(1, 28), rand.Next(1, 2), 2019);
            _termin_zwrotu = Date.getDate(rand.Next(1, 30), rand.Next(11, 12), 2019);
            _data_oddania = Date.getDate(rand.Next(1, 30), rand.Next(3, 10), 2019);
        }
    }

    public class DatabaseHUB
    {
        public DatabaseHUB()
        {
            //"DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;" + "USER ID=s93477;PASSWORD=dzban1436;";
            ConnectionString = "DATA SOURCE=217.173.198.135:1522/orcltp.iaii.local;USER ID=s93340;PASSWORD=s93340;";
            CommandType = System.Data.CommandType.Text;
        }

        private string _connectionString;
        private System.Data.CommandType _commandType;
        private string _queryString;

        public string ConnectionString { get => _connectionString; set => _connectionString = value; }
        public CommandType CommandType { get => _commandType; set => _commandType = value; }
        public string QueryString { get => _queryString; set => _queryString = value; }

        public string Query(string str)
        {
            string returnedString = "";
            OracleConnection con = new OracleConnection();
            con.ConnectionString = _connectionString;
            con.Open();
            OracleCommand cmd = new OracleCommand(str, con);
            cmd.CommandType = _commandType;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                returnedString = reader.GetDecimal(0).ToString();
            }
            con.Close();
            return returnedString;
        }

        public string Query()
        {
            string returnedString = "";
            if (_queryString == "")
            {
                return returnedString;
            }
            OracleConnection con = new OracleConnection();
            con.ConnectionString = _connectionString;
            con.Open();
            OracleCommand cmd = new OracleCommand(_queryString, con);
            cmd.CommandType = _commandType;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                returnedString = reader.GetDecimal(0).ToString();
            }
            con.Close();
            return returnedString;
        }

        public int GetLastId(string idName, string table)
        {
            int id = 0;
            string str = string.Format("SELECT max({0}) FROM {1}", idName, table);
            OracleConnection con = new OracleConnection();
            con.ConnectionString = _connectionString;
            con.Open();
            OracleCommand cmd = new OracleCommand(str, con);
            cmd.CommandType = _commandType;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                id = (int)reader.GetDecimal(0);
            }
            con.Close();
            return id;
        }

        public DateTime GetDate()
        {
            string data = "";
            string str = "SELECT data_wypozyczenia FROM wypozyczenie ORDER BY id_wypozyczenia DESC FETCH FIRST 1 ROWS ONLY";
            OracleConnection con = new OracleConnection();
            con.ConnectionString = _connectionString;
            con.Open();
            OracleCommand cmd = new OracleCommand(str, con);
            cmd.CommandType = _commandType;
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                data = reader[0].ToString();
            }
            con.Close();
            DateTime date = new DateTime();
            date = DateTime.Parse(data);
            return date;
        }
    }

    public static class FileReader
    {
        public static string[] ReadAllLines(string fileName)
        {
            string[] test = System.IO.File.ReadAllLines(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName, fileName));
            return test;
        }
    }

    public static class Date
    {
        public static string getDate(int day, int month, int year)
        {
            return string.Format("{0:0000}", year) + "/" + string.Format("{0:00}", month) + "/" + string.Format("{0:00}", day);
        }

        public static DateTime GetRandomDate(int startYear, int startRangeInDays, int endRangeInDays)
        {
            Random rand = new Random();
            DateTime date = new DateTime(startYear, 1, 1);
            return date.AddDays(rand.Next(startRangeInDays, endRangeInDays));
        }

        public static DateTime GetRandomDate(DateTime startDate, int startRangeInDays, int endRangeInDays)
        {
            Random rand = new Random();
            return startDate.AddDays(rand.Next(startRangeInDays, endRangeInDays));
        }
    }

    public static class Randomizer
    {
        public static int GetRandomItemFromList(List<int> list)
        {
            int i = 0;
            Random rand = new Random();
            i = list[rand.Next(0, list.Count)];
            return i;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string desktop_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Skrypt.txt");
            uint lines = 0;
            string str_lines;
            int lastID_autorzy;
            int lastID_ksiazka;
            int lastID_egzemplarz;
            int lastID_klient;
            int lastID_wypozyczenie;
            int lastID_pracownik;
            DateTime data_wypozyczenia = new DateTime();
            int dateCountdown = 5;
            string[] names = FileReader.ReadAllLines("Imiona.txt");
            string[] surnames = FileReader.ReadAllLines("Nazwiska.txt");
            List<int> id_autora_list = new List<int>();
            List<int> id_ksiazki_list = new List<int>();
            List<int> id_egzemplarza_list = new List<int>();
            List<int> id_klienta_list = new List<int>();
            List<int> id_wypozyczenia_list = new List<int>();
            List<int> id_pracownika_list = new List<int>();
            FileStream fs = new FileStream(desktop_path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            StreamWriter file = new StreamWriter(fs, System.Text.Encoding.UTF8);

            //POBIERANIE INFORMACJI OD UŻYTKOWNIKA
            do
            {
                Console.WriteLine("PODAJ ILOŚĆ WIERSZY");
                str_lines = Console.ReadLine();
            } while (!uint.TryParse(str_lines, out lines));

            //POBIERANIE DANYCH Z BAZY DANYCH
            DatabaseHUB db = new DatabaseHUB();
            lastID_autorzy = db.GetLastId("id_autora", "autorzy");
            lastID_ksiazka = db.GetLastId("id_ksiazki", "ksiazka");
            lastID_egzemplarz = db.GetLastId("id_egzemplarza", "egzemplarz");
            lastID_klient = db.GetLastId("id_klienta", "klient");
            lastID_wypozyczenie = db.GetLastId("id_wypozyczenia", "wypozyczenie");
            lastID_pracownik = db.GetLastId("id_pracownika", "pracownik");
            data_wypozyczenia = db.GetDate();

            //GENEROWANIE DANYCH
            for(int i = 0; i < lines; i++)
            {
                Autorzy autorzy = new Autorzy();
                autorzy.GenerateRandomData(lastID_autorzy + i, names, surnames);
                file.WriteLine(autorzy.GenerateInsert());
                id_autora_list.Add(lastID_autorzy + i + 1);
            }

            for(int i = 0; i < lines; i++)
            {
                Ksiazka ksiazka = new Ksiazka();
                Random rand = new Random();
                ksiazka.GenerateRandomData(lastID_ksiazka + i, names, id_autora_list);
                file.WriteLine(ksiazka.GenerateInsert());
                for(int j = 0; j < rand.Next(1, 5); j++)
                {
                    id_ksiazki_list.Add(lastID_ksiazka + i + 1);
                }
                id_autora_list.Remove(ksiazka.ID_autora);
            }

            int id_ksiazki_list_length = id_ksiazki_list.Count;

            for(int i = 0; i < id_ksiazki_list_length; i++)
            {
                Egzemplarz egzemplarz = new Egzemplarz();
                egzemplarz.GenerateRandomData(lastID_egzemplarz + i, id_ksiazki_list);
                file.WriteLine(egzemplarz.GenerateInsert());
                id_egzemplarza_list.Add(lastID_egzemplarz + i + 1);
                id_ksiazki_list.Remove(egzemplarz.ID_ksiazki);
            }

            for(int i = 0; i < lines; i++)
            {
                Klient klient = new Klient();
                klient.GenerateRandomData(lastID_klient + i, names, surnames);
                file.WriteLine(klient.GenerateInsert());
                id_klienta_list.Add(lastID_klient + i + 1);
            }

            for(int i = 0; i < lines; i++)
            {
                Pracownik pracownik = new Pracownik();
                pracownik.GenerateRandomData(lastID_pracownik + i, names, surnames);
                file.WriteLine(pracownik.GenerateInsert());
                id_pracownika_list.Add(lastID_pracownik + i + 1);
            }

            for(int i = 0; i < lines; i++)
            {
                Wypozyczenie wypozyczenie = new Wypozyczenie();
                wypozyczenie.GenerateRandomData(lastID_wypozyczenie + i, id_pracownika_list, id_egzemplarza_list, id_klienta_list);
                wypozyczenie.Data_wypozyczenia = data_wypozyczenia.ToString("yy/MM/dd");
                wypozyczenie.Termin_zwrotu = Date.GetRandomDate(data_wypozyczenia, 30, 100).ToString("yy/MM/dd");
                wypozyczenie.Termin_zwrotu = data_wypozyczenia.AddMonths(6).ToString("yy/MM/dd");
                file.WriteLine(wypozyczenie.GenerateInsert());
                id_wypozyczenia_list.Add(lastID_wypozyczenie + i + 1);
                id_pracownika_list.Remove(wypozyczenie.ID_pracownika);
                id_egzemplarza_list.Remove(wypozyczenie.ID_egzemplarza);
                id_klienta_list.Remove(wypozyczenie.ID_klienta);
                dateCountdown--;
                if (dateCountdown == 0) {
                    Random rand = new Random();
                    dateCountdown = rand.Next(5, 15);
                    data_wypozyczenia = data_wypozyczenia.AddDays(1);
                }
            }

            //ZAMYKANIE PLIKU
            file.Flush();
            file.Close();
            Console.WriteLine("PLIK ZOSTAŁ ZAPISANY NA PULPICIE");
            Console.WriteLine("NAZWA PLIKU: Skrypt.txt");
        }
    }
}
